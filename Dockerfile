# DOCKER-VERSION 1.5.0

############################################################
# Dockerfile 
# Based on mtlynch/ingredient-phrase-tagger:nyt-untouched
############################################################

FROM mtlynch/ingredient-phrase-tagger:nyt-untouched

MAINTAINER Jasjeev

RUN apt-get update
RUN apt-get install --yes curl
RUN curl --silent --location https://deb.nodesource.com/setup_4.x | bash -
RUN apt-get install --yes nodejs
RUN apt-get install --yes build-essential

ADD input.txt .
ADD collector.sh .
CMD tail -f /dev/null