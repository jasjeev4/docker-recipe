const request = require('request');

request.get({
    url: "https://api.nytimes.com/svc/search/v2/articlesearch.json",
    qs: {
        'api-key': "f0eef7511ade426aa21a9b456000278d",
        'q': "Indian",
        'fq': "type_of_material:(\"Recipe\")"
    },
}, function(err, response, body) {
    body = JSON.parse(body);
    var docs = body.response.docs;
    for(var i=0; i<docs.length; i++) {
        const doc = docs[i];
        const url = doc.web_url;
        //call parser
        getParsedData(url, function(data) {
            var arr = data.split(/\r?\n/);
            var blanks = 0;
            var ingredients = "";
            for(var j=0; j<arr.length; j++) {
                if(arr[j] == "") {
                    blanks++;
                }
                if(blanks == 2) {
                    ingredients = ingredients + arr[j] + "\n";
                }
            }
            //console.log(ingredients);
            getIngredientsJSON(ingredients, function () {

            });
        });
    }
});

function getParsedData(url, callback) {
    const spawn = require("child_process").spawn;
    const pythonProcess = spawn('python',["Scraper/scrape.py", url]);
    pythonProcess.stdout.on('data', (data) => {
        // Do something with the data returned from python script
        callback(data.toString());
    });
}

function getIngredientsJSON(str, callback) {
    stage1(str, function (data1) {
        stage2(data1, function (result) {
            callback(result);
        })
    })
}

function stage1(str, callback) {
    const spawn = require("child_process").spawn;
    const pythonProcess = spawn('python',["bin/parse-ingredients.py", str]);
    pythonProcess.stdout.on('data', (data) => {
        // Do something with the data returned from python script
        callback(data.toString());
    });
}

function stage2(str, callback) {
    const spawn = require("child_process").spawn;
    const pythonProcess = spawn('python',["bin/convert-to-json.py", str]);
    pythonProcess.stdout.on('data', (data) => {
        // Do something with the data returned from python script
        callback(data.toString());
    });
}