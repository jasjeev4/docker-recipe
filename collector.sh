#!/bin/bash

python bin/parse-ingredients.py $1 > $2/results.txt
python bin/convert-to-json.py $2/results.txt > $2/result.json
